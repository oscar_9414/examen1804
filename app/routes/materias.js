const express = require('express')
// para establecer las distintas rutas, necesitamos instanciar el express router
const router = express.Router()
materiaController = require('../controllers/materiaController')
const auth = require('../middlewares/auth')

router.get('/', (req, res) => {
  materiaController.index(req, res)
})
router.get('/privadas', auth.auth, (req, res) => {
  materiaController.index(req, res)
})

router.post('/', (req, res) => {
  materiaController.create(req, res)
})

router.get('/:id', (req, res) => {
  materiaController.show(req, res)
})

router.delete('/:id', (req, res) => {
  materiaController.remove(req, res)
})

router.put('/:id', (req, res) => {
  materiaController.update(req, res)
})

module.exports = router
