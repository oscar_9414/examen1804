const express = require('express')
const router = express.Router()
const auth = require('../middlewares/auth')
userController = require('../controllers/userController')

router.post('/', (req, res) => {
  userController.register(req, res)
})

module.exports = router
