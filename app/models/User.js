const mongoose = require('mongoose')
const Schema = mongoose.Schema

const UserSchema = new Schema({
  codigo: {
    type: String,
    required: true
  },
  nombre: String,
  password: String
})

// exportamos el esquema correspondiente
module.exports = mongoose.model('User', UserSchema)
//
