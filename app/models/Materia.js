const mongoose = require('mongoose')
const Schema = mongoose.Schema

const materiaSquema = new Schema({
  codigo: {
    type: String,
    required: true
  },
  nombre: String,
  curso: Number,
  horas: Number
})

const materia = mongoose.model('Materia', materiaSquema)

module.exports = materia
