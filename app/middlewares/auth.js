const servicejwt = require('../services/servicejwt')

const auth = function (req, res, next) {
  console.log(req.headers.authorization)
  if (!req.headers.authorization) {
    return res.status(403).send({ message: 'No tienes permiso' })
  }
  const token = req.headers.authorization.split(' ')[1]
  try {
    console.log(payload)
    payload = servicejwt.decodeToken(token)
  } catch (error) {
    return res.status(401).send(`${error}`)
  }
  res.status(200).send({ message: 'con permiso' })
}

module.exports = {
  // esto se hace con el fin de poder usarse en el exterior
  auth
}
