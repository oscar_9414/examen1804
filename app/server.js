//* ****************************************************************** */
//* ****************************************************************** */
//* **** ESTE FICHERO NO DEBE MODIFICARSE                          *** */
//* ****************************************************************** */
//* ****************************************************************** */

const express = require('express') // llamamos a Express
const app = express()
require('./config/mongo')
const bodyParser = require('body-parser')
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

const routerMaterias = require('./routes/materias.js')
const routerUsers = require('./routes/users.js')

const port = process.env.PORT || 8080 // establecemos nuestro puerto

app.use('/materias', routerMaterias)
app.use('/users', routerUsers)

app.get('/', (req, res) => {
  res.json({ mensaje: '¡Hola Mundo!' })
})

// iniciamos nuestro servidor
app.listen(port, () => {
  console.log('API escuchando en el puerto ' + port)
})
//* ****************************************************************** */
//* ****************************************************************** */
//* **** ESTE FICHERO NO DEBE MODIFICARSE                          *** */
//* ****************************************************************** */
//* ****************************************************************** */
